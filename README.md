# Quotes Network (Interface)
> ⚠️ This project is meant for education purposes. **It should never run on a production server**.
> See [Ehsaan/quotes-api](https://gitlab.com/Ehsaan/quotes-api) for backend API.

This is the frontend part of Quotes Network. This is built upon React and [Next.js](https://nextjs.org) and uses [material-ui](https://material-ui.com).

The API server is hardcoded `http://localhost:8080` into the project.

## Installation
Simpler than the backend:
```bash
npm install
npm run dev
```

It runs on `http://localhost:3000` by default.

## Todo
* [ ] E2E tests.
* [ ] `react-infinite-scroller` is buggy, which causes multiple requests to server ([pages/index.js:52](https://gitlab.com/Ehsaan/quotes-ui/blob/master/pages/index.js#L52))
* [ ] Implement a searching function in IMDb when posting a quote.