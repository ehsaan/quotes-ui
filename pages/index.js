import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '../components/AppBar'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import InfiniteScroll from 'react-infinite-scroller'
import fetch from 'isomorphic-unfetch'
import Quotation from '../components/Quotation'

const ITEMS_PER_REQUEST = 25

const styles = theme => ( {
    root: {
        paddingTop: theme.spacing.unit * 10
    }
} )

class Content extends React.Component {
 constructor( props ) {
        super( props )
        
        // Initialize the component with an empty set
        // to prevent errors
        this.state = { list: [], loading: true, hasMoreItems: true }

        // Retrieve the first 25 quotes
        fetch( 'http://localhost:8080/api/quotes?page=0' ).then( async function ( result ) {
            const list = await result.json()
            this.setState( { loading: false } )
            
            // The list is empty, do not fetch the further lists.
            if ( list.length == 0 )
                return this.setState( {
                    hasMoreItems:           false
                } )
            
            this.setState( { list } )
        }.bind( this ) )
    }

    loadMore( page ) {
        // Do it only if we are not loading or not reached the end
        if ( this.state.loading == true || this.state.hasMoreItems == false )
            return
        
        this.setState( { loading: true } )

        // TODO: It looks like that react-infinite-scroller
        // has bugs. It requests pages 2 and 5, although we have
        // said that there is no more items to load.
        fetch( 'http://localhost:8080/api/quotes?page=' + page ).then( async function( result ) {
            var list = this.state.list
            const newItems = await result.json()

            this.setState( { loading: false } )

            if ( newItems.length == 0 )
                return this.setState( {
                    hasMoreItems:       false
                } )
            
            this.setState( {
                list:           [ ...list, ...newItems ]
            } )
        }.bind( this ) )
    }

    render() {
        var items = []

        this.state.list.map( ( item, i ) => {
            items.push(
                <Grid container justify="center" key={ item.id }>
                    <Quotation item={item} key={item.id} />
                </Grid>
            )
        } )

        return (
            <InfiniteScroll
                pageStart={ 0 }
                loadMore={ this.loadMore.bind( this ) }
                hasMore={ this.state.hasMoreItems }
                loader={<Grid container justify="center"><CircularProgress key={0} /></Grid>}>

                { items }

            </InfiniteScroll>
        )
    }
}

class Index extends React.Component {
    render() {
        const { classes } = this.props

        return (
            <div className={classes.root}>
                <AppBar />
                <Content />
            </div>
        )
    }
}

Index.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles( styles )( Index )