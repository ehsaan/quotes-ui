import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import AddCircle from '@material-ui/icons/AddCircle'
import withMobileDialog from '@material-ui/core/withMobileDialog'
import fetch from 'isomorphic-unfetch'

class ContributionDialog extends React.Component {
    constructor( props ) {
        super( props )
        this.state = { open: false, movie: '', text: '', author: '', postDisabled: false }
    }

    handleChange = name => event => {
        this.setState( {
            [ name ] : event.target.value
        } )
    }

    handleOpen = () => {
        this.setState( { open: true } )
    }

    handleClose = () => {
        this.setState( { open: false } )
    }

    handlePost = async () => {
        var { movie, author, text, postDisabled } = this.state
        if ( postDisabled == true ) return

        if ( ! movie || ! author || ! text ) 
            return alert( 'All fields are required' )

        this.setState( { postDisabled: true } )

        // More validation
        try {
            const result = await fetch( 'http://localhost:8080/api/imdb/get/' + movie )
            const data = await result.json()

            if ( data.Response == 'False' ) {
                this.setState( { postDisabled: false } )
                return alert( 'Invalid Movie/Serie ID' )
            }

            await fetch( 'http://localhost:8080/api/quotes', {
                method:         'POST',
                headers:        {
                    'Content-Type': 'application/json'
                },
                body:           JSON.stringify( { imdb: movie, text, author } )
            } )
            location.reload()
        } catch( e ) {
            alert( 'Internal Error' )
        }
    }

    render() {
        const { classes, fullScreen } = this.props

        // TODO: Implement a searching function in IMDB.
        // Backend is prepared, by front-end needs much more effort.
        // That's why I hate front-end.
        return (
            <div>
                <IconButton onClick={this.handleOpen} color="inherit" aria-label="Menu">
                    <AddCircle />
                </IconButton>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    fullScreen={fullScreen}>

                    <DialogTitle>Contribute</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            You can share your favorite quotes from the movies and TV series you've watched.
                        </DialogContentText>

                        <TextField
                            autoFocus
                            id="movie"
                            label="Movie/Series IMDB code"
                            type="text"
                            fullWidth
                            required
                            value={this.state.movie}
                            disabled={this.state.postDisabled}
                            onChange={this.handleChange( 'movie' )} />
                        
                        <TextField
                            id="text"
                            label="Content"
                            type="text"
                            multiline
                            fullWidth
                            required
                            value={this.state.text}
                            disabled={this.state.postDisabled}
                            onChange={ this.handleChange( 'text' ) } />

                        <TextField
                            id="author"
                            label="Your name"
                            type="text"
                            fullWidth
                            required
                            value={this.state.author}
                            disabled={this.state.postDisabled}
                            onChange={ this.handleChange( 'author' ) } />
                            
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose}>
                            Cancel
                        </Button>
                        <Button onClick={this.handlePost} disabled={this.state.postDisabled} color="primary">
                            Post
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

ContributionDialog.propTypes = {
	classes: PropTypes.object.isRequired,
}

export default withMobileDialog()( ContributionDialog )